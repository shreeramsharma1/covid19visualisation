import pandas as pd
import matplotlib.pyplot as plt

confirmed = pd.read_csv('covid19_confirmed.csv')
deaths = pd.read_csv('covid19_deaths.csv')
recovered = pd.read_csv('covid19_recovered.csv')

confirmed = confirmed.drop(['Province/State','Lat','Long'], axis=1)
deaths = deaths.drop(['Province/State','Lat','Long'], axis=1)
recovered = recovered.drop(['Province/State','Lat','Long'], axis=1)

confirmed = confirmed.groupby(confirmed['Country/Region']).aggregate('sum')
deaths = deaths.groupby(deaths['Country/Region']).aggregate('sum')
recovered = recovered.groupby(recovered['Country/Region']).aggregate('sum')

confirmed = confirmed.T
deaths = deaths.T
recovered = recovered.T

new_cases = confirmed.copy()

for day in range(1,len(confirmed)):
    new_cases.iloc[day] = confirmed.iloc[day] - confirmed.iloc[day - 1]
    
growth_rate = confirmed.copy()

for day in range(1,len(confirmed)):
    growth_rate.iloc[day] = (new_cases.iloc[day] / confirmed.iloc[day-1]) * 100

active_cases = confirmed.copy()

for day in range(0,len(confirmed)):
    active_cases.iloc[day] = growth_rate.iloc[day] - confirmed.iloc[day] - recovered.iloc[day]

overall_growth_rate = confirmed.copy()

for day in range(1,len(confirmed)):
    overall_growth_rate.iloc[day] = ((active_cases.iloc[day] - active_cases.iloc[day-1]) / active_cases.iloc[day-1])*100
    
death_rate = confirmed.copy()

for day in range(0,len(confirmed)):
    death_rate.iloc[day] = (deaths.iloc[day] / confirmed.iloc[day]) * 100

hospitalization_rate_estimate = 0.05

hospitalization_needed = confirmed.copy()
for day in range(0,len(confirmed)):
    hospitalization_needed.iloc[day] = active_cases.iloc[day] * hospitalization_rate_estimate

#visualization

countries = ['China','India','Italy','US','Spain','France']


for country in countries:
    ax = plt.subplot()
    ax.set_facecolor('black')
    ax.figure.set_facecolor('#121212')
    ax.tick_params(axis='x',colors='white')
    ax.tick_params(axis='y',colors='white')
    ax.set_title(f'covid-19 Total deaths cases in {country}',color='white')
    #confirmed[country].plot(label='country')
    #death rate rising in countries
    death_rate[country].plot.bar()  

    #plt.show()

simulated_growth_rate = 0.05

dates = pd.date_range(start='3/23/2020',periods=40,freq='D')
dates = pd.Series(dates)
dates = dates.dt.strftime('%m%d%y')

simulated = confirmed.copy()
simulated = simulated.append(pd.DataFrame(index=dates))

for day in range(len(confirmed),len(confirmed)+40):
    simulated.iloc[day] = simulated.iloc[day - 1] * (1 + simulated_growth_rate)

ax = plt.subplot()
ax.set_facecolor('black')
ax.figure.set_facecolor('#121212')
ax.tick_params(axis='x',colors='white')
ax.tick_params(axis='y',colors='white')
ax.set_title('Future simulation for Italy',color='white')
simulated['India'].plot()
plt.show()

